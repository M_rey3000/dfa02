//<editor-fold desc="Description">
package theory02;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import javax.swing.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private Circle cir_q0;
    @FXML
    private Circle cir_q1;
    @FXML
    private Circle cir_q2;
    @FXML
    private Circle cir_qf;
    @FXML
    private Text txt_q0q1;
    @FXML
    private Text txt_q1q2;
    @FXML
    private Text txt_q2q2;
    @FXML
    private Text txt_q2qf;
    @FXML
    private Button btnInput;

    private int cnt_q0q1 = 0;
    private int cnt_q1q2 = 0;
    private int cnt_q2q2 = 0;
    private int cnt_q2qf = 0;

    @FXML
    private void btnInputAction(ActionEvent event) {
        cnt_q0q1 = 0;
        cnt_q1q2 = 0;
        cnt_q2q2 = 0;
        cnt_q2qf = 0;

        machine();
    }


    private void machine() {
        String state = "q0";
        Scanner scan = new Scanner(System.in);
//        System.out.println(">>>Input: ");
        String strInput = JOptionPane.showInputDialog(null, ">>>Input: ", "Input", JOptionPane.QUESTION_MESSAGE);

        for (int i = 0; i < strInput.length(); i++) {
            switch (state) {
                case "q0":
                    if (strInput.charAt(i) == 'a') {
                        state = "q1";
                        cnt_q0q1++;
                    } else {
                        state = "trap";
                    }

                    break;

                case "q1":
                    if (strInput.charAt(i) == 'a') {
                        state = "q2";
                        cnt_q1q2++;
                    } else {
                        state = "trap";
                    }
                    break;

                case "q2":
                    if (strInput.charAt(i) == 'a') {
                        state = "qf";
                        cnt_q2qf++;
                    } else if (strInput.charAt(i) == 'b') {
                        state = "q2";
                        cnt_q2q2++;
                    } else {
                        state = "trap";
                    }
                    break;

                case "qf":
                    state = "trap";
                    break;


            }

        }
        switch (state) {
            //Accepted
            case "qf":
                JOptionPane.showMessageDialog(null, strInput, "Accepted!", JOptionPane.INFORMATION_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.DODGERBLUE);
                cir_qf.setFill(Color.BLUEVIOLET);
                setAcceptInfo();
                break;
            //Not Accepted
            case "q0":
                JOptionPane.showMessageDialog(null, strInput, "NOT Accepted!", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.RED);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.DODGERBLUE);
                cir_qf.setFill(Color.DODGERBLUE);
                break;
            case "q1":
                JOptionPane.showMessageDialog(null, strInput, "NOT Accepted!", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.RED);
                cir_q2.setFill(Color.DODGERBLUE);
                cir_qf.setFill(Color.DODGERBLUE);
                break;
            case "q2":
                JOptionPane.showMessageDialog(null, strInput, "NOT Accepted!", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.RED);
                cir_qf.setFill(Color.DODGERBLUE);
                break;
            default:
                JOptionPane.showMessageDialog(null, "Not Accepted! (Alphabet is (a,b))", "", JOptionPane.ERROR_MESSAGE);
                cir_q0.setFill(Color.DODGERBLUE);
                cir_q1.setFill(Color.DODGERBLUE);
                cir_q2.setFill(Color.DODGERBLUE);
                cir_qf.setFill(Color.DODGERBLUE);
                break;
        }

    }

    private void setAcceptInfo() {
        txt_q0q1.setText("a: " + cnt_q0q1);
        txt_q1q2.setText("a: " + cnt_q1q2);
        txt_q2q2.setText("b: " + cnt_q2q2);
        txt_q2qf.setText("a: " + cnt_q2qf);
    }

}
//</editor-fold>
